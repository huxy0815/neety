package com.puyu;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class StudentNamespaceHandler extends NamespaceHandlerSupport {
    public void init() {
        registerBeanDefinitionParser("student", new StudentBeanDefinitionParser());
    }
}
