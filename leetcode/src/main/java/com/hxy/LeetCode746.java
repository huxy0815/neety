package com.hxy;

public class LeetCode746 {

    public static String reformatNumber(String number) {
        String replace = number.trim().replaceAll("-", "").replaceAll(" ","");
        char[] chars = replace.toCharArray();
        int length = replace.length();
        StringBuilder sb = new StringBuilder();
        int k = 0;
        int j = 0;
        boolean flag = true;
        for (int i = 0; i < length; i++) {
            if(i >= length - 4 && (k == 0  || k == 3)){
                if(k == 3 && flag){
                    sb.append("-");
                    flag = false;
                }
                if(j == 2){
                    if( i == length -1 ){
                        sb.append(chars[i]);
                        j=0;
                        continue;
                    }
                    sb.append("-").append(chars[i]);
                    j=0;
                    continue;
                }
                sb.append(chars[i]);
                j++;
                continue;
            }
            if(k == 3){
                sb.append("-").append(chars[i]);
                k=1;
                continue;
            }
            sb.append(chars[i]);
            k++;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(reformatNumber("175-2293-5394-75"));
    }
}
