package com.netty.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.netty.bean.User;

public interface  UserDao extends  BaseMapper<User> {
}
