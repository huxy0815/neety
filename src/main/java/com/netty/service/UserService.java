package com.netty.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.netty.bean.User;

public interface  UserService   extends IService<User> {
}
