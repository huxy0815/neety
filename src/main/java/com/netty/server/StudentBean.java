package com.netty.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentBean {
    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public StudentBean(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {
        List<StudentBean> list = new ArrayList<StudentBean>();
        list.add(new StudentBean("a","22"));
        list.add(new StudentBean("c","22"));
        list.add(new StudentBean("b","22"));
        List<String> strings = new ArrayList<String>();
        strings.add("a");
        strings.add("b");
        strings.add("c");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).getName().toString());
        }
        Collections.sort(list,((o1, o2) -> {
            int io1 = strings.indexOf(o1.getName());
            int io2 = strings.indexOf(o2.getName());
            return io1 - io2;
        }));
        list.forEach(t-> System.out.print(t.name+" " ));
    }
}
