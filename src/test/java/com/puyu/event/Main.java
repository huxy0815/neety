package com.puyu.event;

import com.netty.AopConfig;
import com.netty.aop.DemoAnnotationService;
import com.netty.aop.DemoMethodService;
import com.netty.event.DemoPublisher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);

        DemoPublisher bean = context.getBean(DemoPublisher.class);

        bean.publish("hello world");

        context.close();
    }
}
