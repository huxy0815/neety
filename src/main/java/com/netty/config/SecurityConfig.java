package com.netty.config;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) {
		// allow Swagger URL to be accessed without authentication
		web.ignoring().antMatchers("/**");
		
//		web.ignoring().antMatchers("/v2/api-docs", // swagger api json
//				"/swagger-resources/configuration/ui", // 用来获取支持的动作
//				"/swagger-resources", // 用来获取api-docs的URI
//				"/swagger-resources/configuration/security", // 安全选项
//				"/swagger-ui.html");
//
//		web.ignoring().antMatchers("/swagger-ui.html")
//		.antMatchers("/webjars/**")
//		.antMatchers("/v2/**")
//		.antMatchers("/swagger-resources/**");
	}

	/**
	 * 新版本的spring-cloud2.0中： Spring
	 * Security默认开启了CSRF攻击防御CSRF会将微服务的注册也给过滤了，虽然不会影响注册中心，但是其他客户端是注册不了的 如下方式关闭csrf攻击:
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().anyRequest().authenticated().and().httpBasic().and().csrf().disable();
		
		//http.authorizeRequests().anyRequest().permitAll().and().logout().permitAll();
	}
}
