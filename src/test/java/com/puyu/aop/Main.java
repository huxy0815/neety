package com.puyu.aop;

import com.netty.AopConfig;
import com.netty.aop.DemoAnnotationService;
import com.netty.aop.DemoMethodService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);

        DemoAnnotationService bean = context.getBean(DemoAnnotationService.class);

        DemoMethodService bean1 = context.getBean(DemoMethodService.class);

        bean.add();
        bean1.add();
        context.close();
    }
}
