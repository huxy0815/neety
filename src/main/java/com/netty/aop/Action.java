package com.netty.aop;

import java.lang.annotation.*;

/**
 * @author 23634
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Action {
    String name();
}
