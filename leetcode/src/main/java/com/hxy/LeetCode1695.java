package com.hxy;

import java.util.*;

public class LeetCode1695 {

    public static int maximumUniqueSubarray(int[] nums) {
        List<Integer> list = new ArrayList<>();
        int max = 0;
        int sum = 0;
        int start = 0;
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if(list.contains(num)){
                int j = list.lastIndexOf(num);
                // 重新遍历
                int min = 0;
                for (int k = start; k <=j; k++) {
                    Integer min1 = list.get(k);
                    min += min1;
                    start ++;

                }
                sum -= min;

            }
            list.add(num);
            sum += num;
            max = Math.max(sum,max);
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println(maximumUniqueSubarray(new int[]{5,2,1,2,5,2,1,2,5}));
    }
}
