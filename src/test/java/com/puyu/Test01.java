package com.puyu;

import com.netty.bean.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;

public class Test01 {
    public static void main(String[] args) throws Exception {
        SqlSessionFactory sqlSessionFactory1=new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("mybatis-mysql-config.xml"),"dev");
        SqlSession sqlSession1= sqlSessionFactory1.openSession(true);
      //  IUserMapper userMapper=sqlSession1.getMapper(IUserMapper.class);
        User user=new User();
       // user.setAge(28);
        //user.setName("a");
     //   int i=userMapper.insertUser(user);
        System.out.println("受影响的行数");
        sqlSession1.close();
    }
    @Test
    public void testBatch() throws IOException {
       /**   SqlSessionFactory sqlSessionFactory = getSqlSessionFactory();

        //可以执行批量操作的sqlSession
        SqlSession openSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        long start = System.currentTimeMillis();
        try{
            IUserMapper mapper = openSession.getMapper(IUserMapper.class);
            for (int i = 0; i < 10000; i++) {
                mapper.addEmp(new Employee(UUID.randomUUID().toString().substring(0, 5), "b", "1"));
            }
            openSession.commit();
            long end = System.currentTimeMillis();
            //批量：（预编译sql一次==>设置参数===>10000次===>执行（1次））
            //Parameters: 616c1(String), b(String), 1(String)==>4598
            //非批量：（预编译sql=设置参数=执行）==》10000    10200
            System.out.println("执行时长："+(end-start));
        }finally{
            openSession.close();
        }
**/
    }
}
