package com.hxy;

import java.util.HashMap;
import java.util.Map;

public class LeetCode205 {

    public boolean isIsomorphic(String s, String t) {
        char[] chars = s.toCharArray();
        char[] chars1 = t.toCharArray();
        Map<Character, Character> s2t = new HashMap<Character, Character>();
        Map<Character, Character> t2s = new HashMap<Character, Character>();
        for (int i = 0; i < chars.length; i++) {
            Character x = chars[i];
            Character y = chars1[i];
            if((s2t.containsKey(x) && s2t.get(x) != y)  || (t2s.containsKey(y) &&  t2s.get(t2s) != x)) {

                return  false;
            }
            s2t.put(x,y);
            t2s.put(y,x);
        }
        return true;

    }
}
