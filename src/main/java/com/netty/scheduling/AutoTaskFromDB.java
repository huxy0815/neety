package com.netty.scheduling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

@Component
public class AutoTaskFromDB implements SchedulingConfigurer {

    @Autowired
    private Test test;
    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addTriggerTask(() -> process(),
                triggerContext -> {
                    String cron = "*/6 * * * * ?";
                    if (cron.isEmpty()) {

                    }
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                }
        );
    }
    private void process(){
        test.test();
    }
}
