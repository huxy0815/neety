package com.puyu.jsr;

import com.netty.AopConfig;
import com.netty.aop.BeanWayService;
import com.netty.aop.DemoAnnotationService;
import com.netty.aop.DemoMethodService;
import com.netty.aop.JSR250WayService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AopConfig.class);

        BeanWayService beanWayService = (BeanWayService) context.getBean("beanWayService");

        JSR250WayService jsr250WayService = (JSR250WayService) context.getBean("jsr250WayService");

        context.close();
    }
}
