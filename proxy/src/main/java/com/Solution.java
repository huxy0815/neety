package com;

/**
 * 如何通过反射来构建对象
 */
public class Solution {
    private String str;
    private int num;

    public Solution(){

    }

    public Solution(String str, int num) {
        this.str = str;
        this.num = num;
    }

    public Solution(String str) {
        this.str = str;
    }

    public static void main(String[] args) throws Exception {
        Solution solution = Solution.class.newInstance();
        Solution solution1 = solution.getClass().newInstance();
        Class<?> solution2 = Class.forName("com.Solution");
        Solution solution3 = (Solution) solution2.newInstance();
        System.out.println(solution instanceof Solution);
        System.out.println(solution1 instanceof Solution);
        System.out.println(solution3 instanceof Solution);
        // 通过参数类型,参数个数去确定具体的构造方法
        Class[] classes = new Class[] {String.class,int.class};
        Solution hello = Solution.class.getConstructor(classes).newInstance("hello", 10);
        System.out.println(hello.str);
        // 这个方法会返回制定参数类型的所有构造器，包括public的和非public的，当然也包括private的。
        Solution hello3 = hello.getClass().getDeclaredConstructor(String.class).newInstance("hello3");
        System.out.println(hello3.str);
        // 通过无参构造方法 去创建对象  这个方法返回的是上面那个方法返回结果的子集，只返回制定参数类型访问权限是public的构造器。
        Solution solution4  = (Solution) Class.forName("com.Solution").getConstructor().newInstance();
        System.out.println(solution4 instanceof Solution);


    }
}
