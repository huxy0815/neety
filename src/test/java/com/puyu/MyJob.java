package com.puyu;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyJob  implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
       // System.out.println(jobExecutionContext.getJobDetail().getJobDataMap().get("myjobMethodName"));
        System.out.println(jobExecutionContext.getJobDetail().getJobDataMap().values());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(sdf.format(new Date()));
    }

    public void test(){
        System.out.println("11111111111");
    }
}
