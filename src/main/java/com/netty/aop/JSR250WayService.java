package com.netty.aop;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author 23634
 */
public class JSR250WayService {
    @PostConstruct
    public void init(){
        System.out.println("JSR250WayService@bean-init");
    }

    public JSR250WayService() {
        super();
        System.out.println("JSR250WayServiceconstranct");
    }
    @PreDestroy
    public void destory(){
        System.out.println("JSR250WayService@bean-destory");
    }
}
