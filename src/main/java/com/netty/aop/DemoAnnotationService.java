package com.netty.aop;

import org.springframework.stereotype.Service;

/**
 * @author 23634
 */
@Service
public class DemoAnnotationService {
    @Action(name = "add操作")
    public void add(){}
}
