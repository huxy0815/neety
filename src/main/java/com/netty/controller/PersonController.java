package com.netty.controller;

import com.netty.service.UserService;
import com.netty.service.UserServiceImpl;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/person")
@Api(tags = "人员接口",description="人员文档说明",hidden=true)
public class PersonController {

  private UserServiceImpl userService;

    
    @RequestMapping(value="selectAll",method=RequestMethod.POST)
    @ApiOperation(value="查询所有的人员",notes="查询所有的人员接口说明")
    @ApiImplicitParams({
            @ApiImplicitParam(name="month",value="年月，格式为：201801",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pageSize",value="页码",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pageNum",value="每页条数",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="empName",value="业务员名称",dataType="String", paramType = "query"),
            @ApiImplicitParam(name="orderType",value="排序类型",dataType="String", paramType = "query"),
    })
    @ApiResponse(response=String.class, code = 200, message = "接口返回对象参数")
    public List<String> selectAll(HttpServletRequest request) {

       // List<Person> list = personService.selectAll();
        return null;
    }

    @RequestMapping(value="findById",method= RequestMethod.POST)
    @ResponseBody
    public String findById(Integer id) {
      //  Person person = personService.findById(id);
        return "person";
    }


}
