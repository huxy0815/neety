package com.hxy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Leetcode217 {
    public static void main(String[] args) {

    }

    public boolean containsDuplicate(int[] nums) {
        if(nums.length <=1){
            return false;
        }
        Set set = new HashSet();
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if(set.contains(num)){
                return true;
            }
            set.add(num);
        }
        return false;
    }
}
