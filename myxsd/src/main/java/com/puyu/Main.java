package com.puyu;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("application.xml");
        Student people = (Student) ctx.getBean("student1");
        System.out.println(people.getId());
        System.out.println(people.getName());
        System.out.println(people.getAge());

    }
}
