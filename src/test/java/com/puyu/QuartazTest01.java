package com.puyu;


import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class QuartazTest01 {


        public static void main( String[] args ) throws SchedulerException, InterruptedException {


                Date date = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                System.out.println("Current Time is:" + simpleDateFormat.format(date));

                //创建一个JobDetail实例， 将该实例与hellojob class 绑定
                JobDetail jobDetail = JobBuilder.newJob(MyJob.class)
                        .withIdentity("myJob").build();  //演示传递参数CronTrigger
                jobDetail.getJobDataMap().put("myjobMethodName","test");
                CronTrigger trigger = (CronTrigger) TriggerBuilder.newTrigger()
                        .withIdentity("myTrigger", "group1")
                        .withSchedule(
                                CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
                        .build();

                //创建Schedule实例
                SchedulerFactory schedulerFactory = new StdSchedulerFactory();
                Scheduler scheduler = schedulerFactory.getScheduler();
                scheduler.start();
                //scheduleJob返回的是最近一次要执行的时间
                System.out.println("scheduled time is:"
                        + simpleDateFormat.format(scheduler.scheduleJob(jobDetail, trigger)));
                      /**  Thread.sleep(2000);
                        scheduler.shutdown(); // 挂起
                        Thread.sleep(3000);
                        scheduler.start();**/
              JobKey jobKey = new JobKey("myJob");
                scheduler.pauseJob(jobKey);
                //System.out.println(scheduler.getTriggerKeys());
                Thread.sleep(3000);
                scheduler.resumeJob(jobKey);
        }

}
