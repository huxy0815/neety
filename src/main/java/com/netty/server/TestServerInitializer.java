package com.netty.server;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

public class TestServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        // ChannelPipeline 拦截链
        ChannelPipeline channelPipeline = socketChannel.pipeline();
        channelPipeline.addLast("httpServerCodec", new HttpServerCodec()); //对于web请求进行编解码作用
        channelPipeline.addLast("testHttpServerHandler", new TestHttpServerHandler());
        channelPipeline.addLast("testHttpServerHandler", new SimpleServerHandler());
    }
}
