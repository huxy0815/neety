package com.hxy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LeetCode49 {
    public List<List<String>> groupAnagrams(String[] strs) {
        if(strs.length == 0){
            return null;

        }
        List<List<String>> listList = new ArrayList<>();
        HashMap<String,List<String>> map = new HashMap<>();
        for (int i = 0; i < strs.length; i++) {
            String str = strs[i];
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            if(map.containsKey(key)){
                map.get(key).add(str);
            }else {
                List<String> list = new ArrayList<>();
                list.add(str);
                map.put(key,list);
            }
        }

        return new ArrayList<List<String>>(map.values());
    }

    public static void main(String[] args) {
        System.out.println("abc".toString().hashCode());
        System.out.println("cba".toString().hashCode());
    }
}
