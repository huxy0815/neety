package com.puyu;

import net.minidev.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class Test02 {

    private Properties props = new Properties();

    public static void main(String[] args) {
        Test02 test02 = new Test02();
        test02.getProperty("spi.impl.SpiImpl1");
    }
    public  String getProperty(String name) {
        String file = "/META-INF/services/spi.SPIService";
        URL fileURL = this.getClass().getResource(file);
        if (fileURL != null) {
            try {
                props.load(this.getClass().getResourceAsStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return props.getProperty(name);
    }
}
