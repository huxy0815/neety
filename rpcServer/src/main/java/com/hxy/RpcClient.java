package com.hxy;


public class RpcClient {

    // 调用服务
    public static void main(String[] args) {
        RpcProxyClient<HelloService> rpcClient = new RpcProxyClient<HelloService>();

        IHello hello = rpcClient.proxyClient(HelloService.class);
        String s = hello.sayHello("dd");
        System.out.println(s);
    }
}
