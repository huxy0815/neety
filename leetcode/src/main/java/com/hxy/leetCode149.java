package com.hxy;

import java.util.HashMap;
import java.util.Map;

public class leetCode149 {

    public int monotoneIncreasingDigits(int N) {
        if(N == 0){
            return N;
        }
        char[] arr = (N + "").toCharArray();
        int max = -1, idx = -1;
        for (int i = 0; i < arr.length - 1; i++) {
            if (max < arr[i]) {
                max = arr[i];
                idx = i;
            }
            if (arr[i] > arr[i + 1]) {
                arr[idx] -= 1;
                for(int j = idx + 1;j < arr.length;j++) {
                    arr[j] = '9';
                }
                break;
            }
        }
        return Integer.parseInt(new String(arr));
    }


    public boolean wordPattern(String pattern, String s) {
        HashMap<Character,String> map = new HashMap<>();
        Map<String, Character> str2ch = new HashMap<String, Character>();
        String[] s1 = s.split(" ");
        if(pattern.length() != s1.length) {
            return false;
        }
        for (int i = 0; i < s1.length; i++) {
            String s2 = s1[i];
            char c = pattern.charAt(i);
            if(map.containsKey(c) || str2ch.containsKey(s2)){
                String s3 = map.get(c);
                Character character = str2ch.get(s2);
                if(!s2.equals(s3)){
                    return false;
                }
                if(!character.equals(c)){
                    return false;
                }
            }else {
                map.put(c,s2);
                str2ch.put(s2,c);
            }


        }
        return true;


    }
}
