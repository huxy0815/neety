package com.netty;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

public class ScheduledExecutorTest {
    private static LongAdder longAdder = new LongAdder();

    public static void main(String[] args) {

        ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();

        scheduledExecutor.scheduleAtFixedRate(ScheduledExecutorTest::doTask,
                1, 1, TimeUnit.SECONDS);
    }

    private static void doTask() {

        int count = longAdder.intValue();
        longAdder.increment();

        System.out.println("定时任务开始执行 === " + count);

        // ① 下面这一段注释前和注释后的区别
        if (count == 3) {
            throw new RuntimeException("some runtime exception");
        }
    }
}
