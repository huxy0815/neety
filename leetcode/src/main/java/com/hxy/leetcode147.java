package com.hxy;

public class leetcode147 {

    public ListNode insertionSortList(ListNode head) {
        if(head == null){
            return null;
        }
        if(head.next == null){
            return head;
        }
      while (head != null){
          int val = head.val;
          if(head.next == null){
              return head;
          }
          int val1 = head.next.val;
          // 置换
          if(val1 < val){
              head.val = val1;
              head.next.val = val;
          }
          head = head.next;
      }
      return head;

    }

}

 class ListNode {
    int val;
      ListNode next;
      ListNode(int x) { val = x; }
 }