package com.hxy;

import com.hxy.bean.AnnotationConfigApplicationContext;
import com.hxy.service.AService;
import com.hxy.service.Bservice;

public class Test {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        AService aService = (AService) annotationConfigApplicationContext.getBean("a");
        aService.query();
        Bservice bservice = (Bservice) annotationConfigApplicationContext.getBean("b");
        bservice.query1();
    }
}
