package com;

import com.MyService;

/**
 * 静态代理
 */
public class StaticProxy implements MyService {
    private MyService target;

     public StaticProxy(MyService target){
         this.target = target;
     }


    public void test01() {
        System.out.println("proxy print log for test01");
        target.test01();
    }


    public void test02(String s) {
        System.out.println("proxy print log for test02");
        target.test02(s);
    }
}
