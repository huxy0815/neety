import com.zodiac.flow.component.handler.Handler;
import com.zodiac.flow.context.Context;
import com.zodiac.flow.context.operate.Operate;
import com.zodiac.flow.context.operate.Operates;
import com.zodiac.flow.context.operate.common.SetValueOperate;
import org.springframework.stereotype.Component;

@Component("start")
public class StartHandler implements Handler {
	@Override
	public String getName() {
		return "start";
	}

	@Override
	public Operate handle(Context context) throws Exception {
		System.out.println("start");
		Integer key1 = context.getInteger("key1");
		System.out.println(key1);//获取数据
		return new SetValueOperate("start-key", String.valueOf(key1))//传递数据
				.tail(Operates.handleFinish());
	}

	@Override
	public Operate receiveMsg(Context context) {
		return null;
	}

	@Override
	public Operate revert(Context context) {
		return null;
	}
}
