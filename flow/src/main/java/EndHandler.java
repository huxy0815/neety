import com.zodiac.flow.component.handler.Handler;
import com.zodiac.flow.context.Context;
import com.zodiac.flow.context.operate.Operate;
import com.zodiac.flow.context.operate.Operates;
import org.springframework.stereotype.Component;

@Component("end")
public class EndHandler implements Handler {
	@Override
	public String getName() {
		return "end";
	}

	@Override
	public Operate handle(Context context) throws Exception {
		System.out.println("end");
		String s = context.get("start-key");//获取之前传递的数据
		System.out.println(s);
		return Operates.handleFinish();
	}

	@Override
	public Operate receiveMsg(Context context) {
		return null;
	}

	@Override
	public Operate revert(Context context) {
		return null;
	}
}
