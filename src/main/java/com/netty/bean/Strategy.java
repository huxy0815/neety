package com.netty.bean;

abstract class Strategy {
    // 算法方法
    abstract void algorithmInterface();
}
class StrategyA extends Strategy {

    @Override
    void algorithmInterface() {
        System.out.println("StrategyA");
    }
}
class StrategyB extends Strategy {

    @Override
    void algorithmInterface() {
        System.out.println("StrategyB");
    }
}
class StrategyC extends Strategy {

    @Override
    void algorithmInterface() {
        System.out.println("StrategyC");
    }
}
// 使用上下文维护算法策略
class Context {
    Strategy strategy;
    public Context(Strategy strategy) {
    this.strategy = strategy;
    }
    public void algorithmInterface() {
    strategy.algorithmInterface();
    }
}
class ClientTestStrategy {
    public static void main(String[] args) {
        Context context;
        context = new Context(new StrategyA());
        context.algorithmInterface();
        context = new Context(new StrategyB());
        context.algorithmInterface();
        context = new Context(new StrategyC());
        context.algorithmInterface();
    }
}