package com.netty.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantWriteLockTest {
    public static void main(String[] args) {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock.writeLock();
        readLock.lock();
        readLock.unlock();

        writeLock.lock();
        writeLock.unlock();


    }
}
