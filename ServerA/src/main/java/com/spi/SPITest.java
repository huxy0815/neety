package com.spi;

import sun.misc.Service;

import java.util.Iterator;
import java.util.ServiceLoader;

public class SPITest {
    public static void main(String[] args) {
        Iterator<SPIService> providers = Service.providers(SPIService.class);
        while (providers.hasNext()) {
            SPIService spiService = providers.next();
            spiService.execute();
        }

        ServiceLoader<SPIService> load = ServiceLoader.load(SPIService.class);
        System.out.println("11111111111111111111111111111111111111");
        Iterator<SPIService> iterator = load.iterator();

        while (iterator.hasNext()) {
            SPIService spiService = iterator.next();
            spiService.execute();
        }
    }
}
