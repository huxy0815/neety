package com.spi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SerApp {
    public static void main(String[] args) {
        SpringApplication.run(SerApp.class, args);
    }
}
