import com.zodiac.flow.dispatcher.DispatcherListener;
import com.zodiac.flow.dispatcher.request.StartProcedureRequest;
import com.zodiac.flow.dispatcher.response.ProcedureResponse;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.Semaphore;

public class Boot {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext application = new ClassPathXmlApplicationContext("flow.xml");
		application.start();
		DispatcherListener listener = new DispatcherListener();
		listener.start();
		//StartProcedureRequest request = StartProcedureRequest.builder().procedureCode("testProcedure").build();
		//输入数据
	//	request.put("key1", "22222");
	//	ProcedureResponse response = listener.dispatch(request);
	//	request.put("key1","2222");
		//listener.dispatch(request);

		final Semaphore semp = new Semaphore(20);


		for (int index = 0; index < 20; index++) {
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						semp.acquire();
						long id = Thread.currentThread().getId();
						System.out.println("111111111111111111111111"+id);
						StartProcedureRequest request = StartProcedureRequest.builder().procedureCode("testProcedure").build();
						request.put("key1", String.valueOf(id));
						listener.dispatch(request);
						semp.release();

						System.out.println("-----------------"+semp.availablePermits());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t1.start();
		}

		try {
			listener.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
