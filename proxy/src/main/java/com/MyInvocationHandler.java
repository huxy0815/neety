package com;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class MyInvocationHandler implements InvocationHandler {

    private MyService target;

    public MyInvocationHandler(MyService target){
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object invoke = method.invoke(target, args);
        System.out.println("proxy invoke");
        if(method.getReturnType().equals(Void.TYPE)){
            return null;
        }else{
            System.out.println(invoke);
            return invoke+"proxy";
        }
    }

    public static void main(String[] args) {
        MyServiceImpl target = new MyServiceImpl();
        Class<? extends MyServiceImpl> aClass = target.getClass();
        MyService proxyInstance = (MyService) Proxy.newProxyInstance(aClass.getClassLoader(), aClass.getInterfaces(), new MyInvocationHandler(target));
        proxyInstance.test01();

    }
}
