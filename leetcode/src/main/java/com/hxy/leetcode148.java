package com.hxy;

public class leetcode148 {
    public ListNode1 sortList(ListNode1 head) {
        if(head == null || head.next == null){
            return head;
        }

        ListNode1 next = head;
        while (next.next != null){
            int min = next.val;
            if(next != null) {
                ListNode1 next1 = next.next;
                while (next1 != null) {
                    int val1 = next1.val;
                    if (val1 < min) {
                        min = val1;
                        next.val = val1;
                        next1.val = min;
                    }
                    next1 = next1.next;
                }
            }
            next  = next.next;
        }



        return head;
    }
}
class ListNode1 {
      int val;
    ListNode1 next;
    ListNode1() {}
    ListNode1(int val) { this.val = val; }
    ListNode1(int val, ListNode1 next) { this.val = val; this.next = next; }
  }
